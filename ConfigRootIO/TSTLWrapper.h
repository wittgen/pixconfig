#ifndef _STLWrapperObject_h_
#define _STLWrapperObject_h_

#include <TObject.h>
#include <iostream>
#include <vector>

/** Wraps stl container of simple types : floats, ints, bools etc.
 * Strings which are not simple types are also supported (vectors of strings
 * and single strings.
 */
template <class T>
class STLWrapperObject : public TObject
{
public:
  STLWrapperObject(T *stl_container)
    : m_ptr(stl_container),
      m_owner(false)
  { /*std::cout << "INFO [STLWrapperObject::ctor] use given object " << static_cast<void *>(stl_container) << ". adr=" << static_cast<void *>(this) << std::endl; */}

  STLWrapperObject()
    : m_ptr(new T),
      m_owner(true)
  {  /*std::cout << "INFO [STLWrapperObject::ctor] create STL object. adr=" << static_cast<void *>(this)<< std::endl;*/ }

  STLWrapperObject(STLWrapperObject &obj) :m_ptr(NULL), m_owner(false)
  {
    if (obj.m_owner ) {
      m_owner=true;
      if (obj.m_ptr) {
	m_ptr=new T(*(obj.m_ptr));
      }
      else {
	m_ptr=new T;
      }
      //      std::cout << "INFO [STLWrapperObject::copy_ctor] use given object." << std::endl;
    }
    else {
      m_ptr = obj.m_ptr ;
      m_owner = false;
      //      std::cout << "INFO [STLWrapperObject::copy_ctor] use object of source object." << std::endl;
    }
  }

  ~STLWrapperObject()
  { if (m_owner) { /*std::cout << "INFO [STLWrapperObject::dtor] delete object adr=" << static_cast<void *>(this) << std::endl; */ delete m_ptr; m_ptr=NULL;}}

  const T *ptr() const { return m_ptr; }

  void Copy(TObject &obj) const;

protected:

  T *m_ptr;
  bool m_owner;
  ClassDef(STLWrapperObject,1)
};

typedef  STLWrapperObject<std::string> STLStringWrapperObject;

typedef STLWrapperObject< std::vector<int> >           STLVectorIntWrapperObject;
typedef STLWrapperObject< std::vector<unsigned int> >  STLVectorUIntWrapperObject;
typedef STLWrapperObject< std::vector<float> >         STLVectorFloatWrapperObject;
typedef STLWrapperObject< std::vector<std::string> >   STLVectorStringWrapperObject;

#endif

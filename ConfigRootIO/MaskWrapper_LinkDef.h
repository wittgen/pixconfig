#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link off all typedefs;

#pragma link C++ class MaskWrapperObject<bool>-;
#pragma link C++ class MaskWrapperObject<unsigned short>-;

#endif

#ifndef _MaskWrapperObject_h_
#define _MaskWrapperObject_h_

#include <TObject.h>
#include <iostream>

namespace PixLib {
  template <class T>
  class ConfMask;
}

template <class T>
class MaskWrapperObject : public TObject
{
public:
  MaskWrapperObject(PixLib::ConfMask<T> *mask) : m_ptr(mask), m_owner(false) { /*std::cout << "INFO [MaskWrapperObject::ctor] use given object." << std::endl;*/ }
  MaskWrapperObject() : m_ptr(NULL), m_owner(true) { /*std::cout << "INFO [MaskWrapperObject::ctor] create Mask object" << std::endl; */ }
  MaskWrapperObject(MaskWrapperObject &obj);
  ~MaskWrapperObject();

  const PixLib::ConfMask<T> *ptr() const { return m_ptr; }

  void Copy(TObject &obj) const;

protected:
 
  PixLib::ConfMask<T> *m_ptr;
  bool m_owner;
  ClassDef(MaskWrapperObject,1)
};

typedef MaskWrapperObject< bool >              MaskU1WrapperObject;
typedef MaskWrapperObject< unsigned short >    MaskU16WrapperObject;

#endif

#include "Config/ConfMask.h"
template<> unsigned int PixLib::ConfMask<bool>::counter = { 0 };
template<> unsigned int PixLib::ConfMask<unsigned short int>::counter = { 0 };
template<> double PixLib::ConfMask<bool>::size = { 0.0 };
template<> double PixLib::ConfMask<unsigned short int>::size = { 0.0 };

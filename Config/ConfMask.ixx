/////////////////////////////////////////////////////////////////////
// ConfMask.ixx
// version 0.1
/////////////////////////////////////////////////////////////////////
//
// 13/04/04  Version 0.1 (CS)
//

#include <iostream>

using namespace PixLib;

//! Constructor (all entries are set to zero)
template<class T> ConfMask<T>::ConfMask(int nCol, int nRow, T maxValue) : m_maxValue(maxValue) {
  // Compute the number of bits required
  m_ncol = nCol;
  m_nrow = nRow;
  m_nbits = 32;
  for (int i=1; i<31; i++) {
    if (maxValue < (0x1<<i)) {
      m_nbits = i;
      break;
    }
  }
  // Check column and row numbers
  if(nCol>0 && nRow>0) {
    // Create mask
    int nw = 32/m_nbits;
    int nel = nCol*nRow/nw+1;
    m_mask.clear();
    for (int i=0; i<nel; i++) m_mask.push_back(0);
    size += 4*m_mask.size();
  }
  counter ++;
  //std::cout << "++ count = " << counter <<  " size = " << size << std::endl;
}

//! Constructor (all entries are set to defValue)
template<class T> ConfMask<T>::ConfMask(int nCol, int nRow, T maxValue, T defValue) : m_maxValue(maxValue) {
  // Compute the number of bits required
  m_ncol = nCol;
  m_nrow = nRow;
  m_nbits = 32;
  for (int i=1; i<31; i++) {
    if (maxValue < (0x1<<i)) {
      m_nbits = i;
      break;
    }
  }
  // Check column and row numbers
  if(nCol>0 && nRow>0) {
    // Create mask
    int nw = 32/m_nbits;
    int nel = nCol*nRow/nw+1;
    m_mask.clear();
    for (int i=0; i<nel; i++) m_mask.push_back(0);
    T defaultValue;
    if(defValue<m_maxValue) defaultValue = defValue; else defaultValue = m_maxValue;
    setAll(defaultValue);
    size += 4*m_mask.size();
  }
  counter++;
  //std::cout << "++ count = " << counter <<  " size = " << size << std::endl;
}

//! Copy constructor
template<class T> ConfMask<T>::ConfMask(const ConfMask<T> &c) {
  // Copy mask
  m_mask = c.m_mask;

  // Copy max value
  m_maxValue = c.m_maxValue;

  // Copy number of bits, rows, columns
  m_nbits = c.m_nbits;
  m_ncol = c.m_ncol;
  m_nrow = c.m_nrow;

  size += 4*m_mask.size();
  counter++;
  //std::cout << "+C count = " << counter <<  " size = " << size << std::endl;
}

//! Destructor
template<class T> ConfMask<T>::~ConfMask() {
  // Clear mask
  size -= 4*m_mask.size();
  m_mask.clear();
  counter--;
  //std::cout << "== count = " << counter <<  " size = " << size << std::endl;
}


//! Assignment operator
template<class T> ConfMask<T>& ConfMask<T>::operator = (const ConfMask<T>& c) {
  // Copy mask
  size -= 4*m_mask.size();
  m_mask = c.m_mask;
  size += 4*m_mask.size();

  // Copy max value
  m_maxValue = c.m_maxValue;

  // Copy number of bits, rows, columns
  m_nbits = c.m_nbits;
  m_ncol = c.m_ncol;
  m_nrow = c.m_nrow;

  //std::cout << "CC count = " << count <<  " size = " << size << std::endl;

  return *this;
}

template<class T> void ConfMask<T>::dumpCounters() {
  //std::cout << "ConfMask: count = " << counter <<  " size = " << size << std::endl;
}

//! Set all entries to m_maxValue
template<class T> void ConfMask<T>::enableAll() {
  // Enable mask
  setAll(m_maxValue);
}

//! Set all entries to zero
template<class T> void ConfMask<T>::disableAll() {
  // Disable mask
  setAll(0);
}

//! Set all entries to value
template<class T> void ConfMask<T>::setAll(T value) {
  // Set mask
  T val;
  if(value<m_maxValue) val = value; else val = m_maxValue;
  for (int ic=0; ic<m_ncol; ic++) {
    for (int ir=0; ir<m_nrow; ir++) {
      set(ic, ir, val);
    }
  }
}
  

//! Set an entire column to m_maxValue
template<class T> void ConfMask<T>::enableCol(int col) {
  // Check column number
  if(col<0 || col>=m_ncol) return;

  // Enable column
  for (int ir=0; ir<m_nrow; ir++) {
    set(col, ir, m_maxValue);
  }
}

//! Set an entire column to zero
template<class T> void ConfMask<T>::disableCol(int col) {
  // Check column number
  if(col<0 || col>=m_ncol) return;

  // Disable column
  for (int ir=0; ir<m_nrow; ir++) {
    set(col, ir, 0);
  }
}

//! Set an entire column to value
template<class T> void ConfMask<T>::setCol(int col, T value) {
  // Check column number
  if(col<0 || col>=m_ncol) return;

  // Set column to value
  T val;
  if(value<m_maxValue) val = value; else val = m_maxValue;
  for (int ir=0; ir<m_nrow; ir++) {
    set(col, ir, val);
  }
}


//! Set an entire row to m_maxValue
template<class T> void ConfMask<T>::enableRow(int row) {
  // Check row number
  if(row<0 || row>=m_nrow) return;

  // Enable row
  for (int ic=0; ic<m_ncol; ic++) {
    set(ic, row, m_maxValue);
  }
}

//! Set an entire row to zero
template<class T> void ConfMask<T>::disableRow(int row) {
  // Check row number
  if(row<0 || row>=m_nrow) return;

  // Disable row
  for (int ic=0; ic<m_ncol; ic++) {
    set(ic, row, 0);
  }
}

//! Set an entire row to value
template<class T> void ConfMask<T>::setRow(int row, T value) {
  // Check row number
  if(row<0 || row>=m_nrow) return;

  // Set row to value
  T val;
  if(value<m_maxValue) val = value; else val = m_maxValue;
  for (int ic=0; ic<m_ncol; ic++) {
    set(ic, row, val);
  }
}


//! Set entry to m_maxValue
template<class T> void ConfMask<T>::enable(int col, int row) {
  // Check row and column number
  if(col<0 || col>=m_ncol) return;
  if(row<0 || row>=m_nrow) return;

  // Enable entry
  set(col, row, m_maxValue);
}

//! Set entry to m_maxValue
template<class T> void ConfMask<T>::disable(int col, int row) {
  // Check row and column number
  if(col<0 || col>=m_ncol) return;
  if(row<0 || row>=m_nrow) return;

  // Disable entry
  set(col, row, 0);
}

//! Set entry to m_maxValue
template<class T> void ConfMask<T>::set(int col, int row, T value) {
  // Check row and column number
  if(col<0 || col>=m_ncol) return;
  if(row<0 || row>=m_nrow) return;

  // Set entry to value
  int nel = 32/m_nbits;
  int pos = (col*m_nrow+row)/nel;
  int off = ((col*m_nrow+row)%nel)*m_nbits;
  T val;
  if(value<m_maxValue) val = value; else val = m_maxValue;
  unsigned int uval = (val & ((0x1<<m_nbits)-1))<<off;
  unsigned int mask = ~(((0x1<<m_nbits)-1)<<off);
  if (pos<(int)m_mask.size()) { 
    m_mask[pos] &= mask;
    m_mask[pos] |= uval;
  } else {
    std::cout << "++++ Error : invalid pos = " << pos << std::endl;
  }
}


//! Initialization from vector of T
template<class T> void ConfMask<T>::set(std::vector<T> &value) {
  int col=0, row=0;

  // Set entries
  for(unsigned int i=0; i<value.size() && col<m_ncol && row<m_nrow; i++) {
    set(col, row, value[i]);
    col++; if(col==m_ncol) {row++; col=0;}
  }
}

//! Output to vector of T
template<class T> void ConfMask<T>::get(std::vector<T> &output) {

  // Get entries
  output.clear();
  for(int row=0; row<m_nrow; row++) {
    for(int col=0; col<m_ncol; col++) {
      output.push_back(get(col, row));
    }
  }
}

//! Mask accessor method
template<class T> std::vector< std::vector<T> > ConfMask<T>::get() {
  std::vector< std::vector<T> > v;
  for (int ic=0; ic<m_ncol; ic++) {
    v.push_back(std::vector<T>());
    for (int ir=0; ir<m_nrow; ir++) {
      v[ic].push_back(get(ic, ir));
    }
  }
  return v;
}

//! Column accessor method
template<class T> std::vector<T> ConfMask<T>::get(int col) {
  std::vector<T> v;
  if (col<0 || col>=m_ncol) return v;
  for (int ir=0; ir<m_nrow; ir++) {
    v.push_back(get(col, ir));
  }
  return v;
}

//! Mask accessor method

template<class T> T ConfMask<T>::get(int col, int row) {
  if (col<0 || col>=m_ncol) return 0;
  if (row<0 || row>=m_nrow) return 0;
  int nel = 32/m_nbits;
  int pos = (col*m_nrow+row)/nel;
  int off = ((col*m_nrow+row)%nel)*m_nbits;
  return (m_mask[pos]>>off)&((0x1<<m_nbits)-1);
}



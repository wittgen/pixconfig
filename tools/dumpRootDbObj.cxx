#include "TKey.h"
#include "TTree.h"
#include "TLeaf.h"
#include "TBranch.h"
#include "TSystem.h"
#include "TDirectory.h"
#include "TFile.h"
#include "TLeafObject.h"
#include <iostream>
#include <signal.h>
#include <Config/ConfMask.h>
#include <ConfigRootIO/TSTLWrapper.h>
#include <ConfigRootIO/MaskWrapper.h>
#include <variant.hpp>

using namespace std;
using namespace PixLib;
void split_str(std::vector<std::string> &res,const std::string &s,std::string delim=".") {
  auto start = 0U;
  auto end = s.find(delim);
  while (end != std::string::npos)
    {
      res.push_back(s.substr(start, end - start));
      start = end + delim.length();
      end = s.find(delim, start);
    }
  
  res.push_back( s.substr(start, end));
}

void DumpRootDbInDir(TDirectory *cfgDir,variant32 &var);

void DumpRootDbObj(const char* fileName,variant32 &var) {
  //	std::cout << "File name: " << fileName << std::endl;
	gSystem->Load("$PIXCONF/$CMTCONFIG/libPixConfig.so");
	TFile *f = TFile::Open(fileName);
	std::string fName = fileName;
	fName = fName.substr(fName.find_last_of("/") + 1, fName.size() - fName.find_last_of("/") - 1);
	fName = fName.substr(0, fName.find("_00"));
	TDirectory* cfgDir = static_cast<TDirectory*>( f->GetDirectory(fName.c_str()) );
	if(!cfgDir) cfgDir = static_cast<TDirectory*>( f->GetDirectory("Def") );
	if(!cfgDir) std::cerr << "Error: cannot find directory" << std::endl;
	else DumpRootDbInDir(cfgDir, var);
	DumpRootDbInDir(gDirectory, var);
	f->Close();
}

void PrintTree(TTree* t,variant32 &var) {
  TIter next( t->GetListOfBranches() );
  TBranch *br;
  while( (br = static_cast<TBranch*>( next() )) ) {
    if(br->GetListOfLeaves()->GetEntries()!=1) continue;
    TLeaf *l=(TLeaf *)(br->GetListOfLeaves())->UncheckedAt(0);    
    std::string typeName(l->GetTypeName());
    std::string brName = br->GetName();
    std::string brTitle = br->GetTitle();
    //    std::cout << "brTitle="<<brTitle<< std::endl;
    //std::cout << "brName="<<br->GetName()<< std::endl;
    std::vector<std::string> path;
    split_str(path,brName);
    std::size_t plen=path.size();
    variant32 value;
    //    std::cout << std::endl;
    // std::cout << "typeName="<<typeName<< std::endl;
    if(typeName=="Int_t") {
      int32_t v; br->SetAddress(&v); br->GetEntry(0);
      value=v;
    } else if (typeName=="Uint_t") {
      uint32_t v ; br->SetAddress(&v); br->GetEntry(0);
      value=v;
    } else if (typeName=="Float_t") {
      float v ; br->SetAddress(&v); br->GetEntry(0);
      value=v;
    }  else if (typeName=="Char_t") {
      bool v;  br->SetAddress(&v); br->GetEntry(0);
      value=v;
    } else if (typeName=="STLWrapperObject<string>") {
      STLWrapperObject<string> * v=0;
      br->SetAddress(&v);
      br->GetEntry(0);
      value=std::string(*v->ptr());
    } else if(typeName=="MaskWrapperObject<bool>") {
      MaskWrapperObject<bool> *v=0;
      br->SetAddress(&v);
      br->GetEntry(0);
      ConfMask<bool> *cm=(ConfMask<bool> *)  v->ptr();
      value["ConfMask"]["type"]="bool";
      value["ConfMask"]["ncol"]=(uint32_t) cm->ncol();     
      value["ConfMask"]["nrow"]=(uint32_t) cm->nrow();
      std::vector<unsigned int> &mask=cm->getMask();
      std::vector<uint32_t> newmask;      
      for (const auto &el:mask) newmask.push_back(el);      
      value["ConfMask"]["data"]=newmask; 
    } else if(typeName=="MaskWrapperObject<unsigned short>") {
      MaskWrapperObject<unsigned short> *v=0;
      br->SetAddress(&v);
      br->GetEntry(0);
      ConfMask<unsigned short> *cm=( ConfMask<unsigned short> *)v->ptr();      
      value["ConfMask"]["type"]="ushort";      
      value["ConfMask"]["ncol"]=(uint32_t) cm->ncol();      
      value["ConfMask"]["nrow"]=(uint32_t) cm->nrow();
      std::vector<unsigned int> &mask=cm->getMask();  
      std::vector<uint32_t> newmask;      
      for (const auto &el:mask) newmask.push_back(el);      
      value["ConfMask"]["data"]=newmask;
    }
    if(plen==4)
      var[path[0]][path[1]][path[2]][path[3]]=value;   
    else if(plen==3) 
      var[path[0]][path[1]][path[2]]=value;
    else if(plen==2)
      var[path[0]][path[1]]=value;
    else if(plen==1) {
      var[path[0]]=value;
    } else throw;
    
  }
}

void DumpRootDbInDir(TDirectory *cfgDir,variant32 &var) {
  TIter next( cfgDir->GetListOfKeys() );
  TKey* key;
  while( (key = static_cast<TKey*>( next() )) ) {
    key->Print();
    if( key->ReadObj()->InheritsFrom("TDirectory") )
      DumpRootDbInDir( (TDirectory*) key->ReadObj(), var );
    else if( key->ReadObj()->InheritsFrom("TTree") ) {
      TTree* t = (TTree*) key->ReadObj();
      PrintTree( t, var );
    }
  }
}

int main(int argc, const char** argv) {
  variant32 var; // variant to dump ROOT into
  const char *fName;
  if(argc>=2) fName = argv[1];
  int nObj = std::numeric_limits<int>::max();
  DumpRootDbObj(fName,var);
  var.dump();
  

  return 0;
	
}

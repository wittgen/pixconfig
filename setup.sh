source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh  tdaq-08-03-01
export PIXCONF=$PWD
export PATH=$ROOTSYS/bin:${PIXCONF}/${CMTCONFIG}:$PATH
export DAVIX_LIB_PATH=${LCG_INST_PATH}/${TDAQ_LCG_RELEASE}/Davix/0.7.1/${CMTCONFIG}/lib64
export LD_LIBRARY_PATH=${DAVIX_LIB_PATH}:${PIXCONF}/${CMTCONFIG}:$LD_LIBRARY_PATH


SUBDIRS = Config ConfigRootIO
LFLAGS += $(shell root-config --ldflags)
LFLAGS += $(shell root-config --libs )


all: libs tools

tools: libs
	make -C tools

subdirs:
	for dir in $(SUBDIRS); do \
	$(MAKE) -C $$dir; \
	done

subdirs_clean:
	for dir in $(SUBDIRS); do \
        $(MAKE) -C $$dir clean; \
        done


libobj=  \
./ConfigRootIO/$(CMTCONFIG)/MaskWrapper_Dict.o \
./ConfigRootIO/$(CMTCONFIG)/MaskWrapper.o \
./ConfigRootIO/$(CMTCONFIG)/TSTLWrapper_Dict.o \
./ConfigRootIO/$(CMTCONFIG)/TSTLWrapper.o \
./Config/$(CMTCONFIG)/ConfigLib.o \
./Config/$(CMTCONFIG)/Config_Dict.o





libPixConfig.so:  subdirs $(libobj)
	if test ! -e $(CMTCONFIG); then mkdir -p $(CMTCONFIG); fi
	$(CXX) $(LFLAGS) -shared   $(shell root-config --glibs)  -Wl,-E  -o $(CMTCONFIG)/libPixConfig.so -dl $(libobj)
	cp Config/*.pcm $(CMTCONFIG)
	cp ConfigRootIO/*.pcm $(CMTCONFIG)

libs: libPixConfig.so 

clean: subdirs_clean
	make -C tools clean
	make -C Config clean
	make -C ConfigRootIO clean
	rm -rf $(CMTCONFIG)
